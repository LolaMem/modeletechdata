DROP DATABASE IF EXISTS techdata;
CREATE DATABASE techdata;
Use techdata;

CREATE TABLE eleves (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    prenom VARCHAR(20),
    nom VARCHAR(50),
    email VARCHAR(50),
    phone VARCHAR(20)
);

CREATE TABLE diplomes (
    id_eleve INT,
    nom VARCHAR(50),
    FOREIGN KEY (id_eleve) REFERENCES eleves(id)
);

CREATE TABLE competences (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(50),
    description VARCHAR(200)
);

CREATE TABLE eleve_competence (
    id_competence INT,
    id_eleve INT,
    niveau enum('1','2','3') NOT NULL DEFAULT '1',
    FOREIGN KEY (id_competence) REFERENCES competences(id),
    FOREIGN KEY (id_eleve) REFERENCES eleves(id),
    UNIQUE (id_competence, id_eleve, niveau)
);

 CREATE TABLE projets (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(200)
);

 CREATE TABLE jours (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    date DATE
);

CREATE TABLE groupe (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    membres INT,
    date_debut int,
    date_fin int,    
    FOREIGN KEY (date_debut) REFERENCES jours(id),
    FOREIGN KEY (date_fin) REFERENCES jours(id)
);

CREATE TABLE groupe_eleve (
    id_eleve INT,
    id_groupe INT,
    FOREIGN KEY (id_eleve) REFERENCES eleves(id),
    FOREIGN KEY (id_groupe) REFERENCES groupe(id)
);

CREATE TABLE projet_groupe (
    id_projet INT,
    id_groupe INT,
    FOREIGN KEY (id_projet) REFERENCES projets(id),
    FOREIGN KEY (id_groupe) REFERENCES groupe(id)
);

CREATE TABLE projet_competence(
    id_competence INT,
    id_projet INT,
    FOREIGN KEY (id_competence) REFERENCES competences(id),
    FOREIGN KEY (id_projet) REFERENCES projets(id)

);

CREATE TABLE matos (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    product VARCHAR(30),
    id_eleve INT,
    FOREIGN KEY (id_eleve) REFERENCES eleves(id)
);

CREATE TABLE jours_absence(
    id_eleve INT,
    id_date INT,
    demo_jour boolean,
    FOREIGN KEY (id_eleve) REFERENCES eleves(id),
    FOREIGN KEY (id_date) REFERENCES jours(id)
);

DROP USER IF EXISTS lolam1@localhost;
CREATE USER lolam1@localhost IDENTIFIED BY 'lolam1';
GRANT ALL PRIVILEGES ON techdata.* TO 'lolam1'@'localhost';

