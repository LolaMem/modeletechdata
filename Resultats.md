# DATA MODELS: CONCEPTUAL & LOGIC/PHISYQUE

## LOCATION VOITURES
### CONDITIONS
* International: agences et clients. Divises
* entretien sous-traitee
* vehicules
### MODELE CONCEPTUELE
![Alt text](loc_voiture.png)


### TABLES ou RELATIONS:
* clients
* adresses
* vehicules: annee fabrication
* contrats + 
* pays, divise
* assurance

 ## TECH DATA
 ### MODELE CONCEPTUAL 
 ![Alt text](tech_data.png)

 ### MODELE PHYSIQUE/LOGIQUE

* Diplome: il y a plusieurs (2) diplome a obtenir pour chaque eleve, independant du competences
* Projets ont une relation many to many avec eleves, a travers du groupe. Les projets sont ces de SimplonLine et hors SimplonLine
* Il y a toujours des groupes. Bien pour de travail individuel (membres=1) ou en equipe. La table groupe garde aussi des dates de debut et fin du groupe/projet, sont equivalent, en tant que relation many to many avec eleves, je met une table intermediaire groupe_eleve
* Les competences sont liees aux eleves (qui les acquisent) et au projet (proposeés/programmés). La table competence enregistre la description et aussi le niveau avec un enum (1, 2, 3). 


![Alt text](DB_TechData.png)

 #### DEPENDANCES FONCTIONNELLES
 * Dans la Table eleves, le email et le phone sont unique et ont une dependence entre eux
 * Dans la Table competences, le email et le phone sont unique et ont une dependence entre eux

Modele physiaye dans le fichier techdata.sql, les donneés dans db_data.sql

 ### TEST SUR BD TECHDATA

**1_nombre de compétence validée par apprenant**

**2_liste des apprenants**
```sql
select * from eleves;

+----+-------------+------------+------------------------------+-------------------+
| id | prenom      | nom        | email                        | phone             |
+----+-------------+------------+------------------------------+-------------------+
|  1 | Milicent    | Opdenort   | mopdenort0@yellowpages.com   | +352 355 502 1250 |
|  2 | Matthias    | Balhatchet | mbalhatchet1@nature.com      | +7 835 820 4527   |
|  3 | Alleyn      | Senter     | asenter2@google.com          | +66 720 243 4650  |
|  4 | Cairistiona | Cuncliffe  | ccuncliffe3@comcast.net      | +86 865 187 5209  |
|  5 | Iorgo       | Hallbord   | ihallbord4@123-reg.co.uk     | +86 596 684 5385  |
|  6 | Arline      | Sarah      | asarah5@stumbleupon.com      | +506 901 455 5274 |
|  7 | Amalia      | Ellum      | aellum6@cam.ac.uk            | +46 703 969 5457  |
|  8 | Cherie      | Joret      | cjoret7@tinypic.com          | +54 148 574 9982  |
|  9 | Peder       | Shrive     | pshrive8@acquirethisname.com | +51 807 888 9694  |
| 10 | Erena       | Hagley     | ehagley9@g.co                | +86 510 239 9657  |
+----+-------------+------------+------------------------------+-------------------+
10 rows in set 
```
**3_Nombre d'absence (jours) par apprenants**

**4_Nombre d'absence (jours) et nombre de compétence validée**
**5_Tout les apprenants d'un groupe**
    nombre de groupe auxquels chaque apprenant a participé
    pour 1 apprenant (avec son nom) la liste des compétences et leur état (validé ou pas)
    le plus absent qui doit ramener des pains au chocolat.
    pour une date donnée la liste des présents.
    liste des apprenants avec leur oridnateur / matériel
    pour un projet la liste des apprenant et leur groupe







 

