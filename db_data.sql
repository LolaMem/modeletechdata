use techdata;
/*--Table eleves*/
insert into eleves (id, prenom, nom, email, phone) values (1, 'Milicent', 'Opdenort', 'mopdenort0@yellowpages.com', '+352 355 502 1250');
insert into eleves (id, prenom, nom, email, phone) values (2, 'Matthias', 'Balhatchet', 'mbalhatchet1@nature.com', '+7 835 820 4527');
insert into eleves (id, prenom, nom, email, phone) values (3, 'Alleyn', 'Senter', 'asenter2@google.com', '+66 720 243 4650');
insert into eleves (id, prenom, nom, email, phone) values (4, 'Cairistiona', 'Cuncliffe', 'ccuncliffe3@comcast.net', '+86 865 187 5209');
insert into eleves (id, prenom, nom, email, phone) values (5, 'Iorgo', 'Hallbord', 'ihallbord4@123-reg.co.uk', '+86 596 684 5385');
insert into eleves (id, prenom, nom, email, phone) values (6, 'Arline', 'Sarah', 'asarah5@stumbleupon.com', '+506 901 455 5274');
insert into eleves (id, prenom, nom, email, phone) values (7, 'Amalia', 'Ellum', 'aellum6@cam.ac.uk', '+46 703 969 5457');
insert into eleves (id, prenom, nom, email, phone) values (8, 'Cherie', 'Joret', 'cjoret7@tinypic.com', '+54 148 574 9982');
insert into eleves (id, prenom, nom, email, phone) values (9, 'Peder', 'Shrive', 'pshrive8@acquirethisname.com', '+51 807 888 9694');
insert into eleves (id, prenom, nom, email, phone) values (10, 'Erena', 'Hagley', 'ehagley9@g.co', '+86 510 239 9657');

/*--Table diplomes*/
INSERT INTO diplomes (id_eleve, nom) VALUES (1, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (2, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (3, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (4, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (5, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (6, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (7, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (9, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (10, 'SAP');
INSERT INTO diplomes (id_eleve, nom) VALUES (1, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (2, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (4, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (5, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (6, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (8, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (9, 'Tech Data');
INSERT INTO diplomes (id_eleve, nom) VALUES (10, 'Tech Data');

/*--Table projets: 25 entries*/
INSERT INTO projets (id, nom) VALUES (1, 'Venus Wars (Venus Senki)');
INSERT INTO projets (id, nom) VALUES (2, 'Torso');
INSERT INTO projets (id, nom) VALUES (3, 'Unknown, The (a.k.a. Alonzo the Armless)');
INSERT INTO projets (id, nom) VALUES (4, 'Perfect Human, The (Perfekte Menneske, Det)');
INSERT INTO projets (id, nom) VALUES (5, 'Infernal Affairs 2 (Mou gaan dou II)');
INSERT INTO projets (id, nom) VALUES (6, 'Quare Fellow, The (a.k.a. The Condemned Man)');
INSERT INTO projets (id, nom) VALUES (7, 'Young at Heart (a.k.a. Young@Heart)');
INSERT INTO projets (id, nom) VALUES (8, 'Casting Couch');
INSERT INTO projets (id, nom) VALUES (9, 'Letter From Death Row, A');
INSERT INTO projets (id, nom) VALUES (10, 'Possible Loves (Amores Possíveis)');
INSERT INTO projets (id, nom) VALUES (11, 'Porto of My Childhood (Porto da Minha Infância)');
INSERT INTO projets (id, nom) VALUES (12, 'Man in the Moon, The');
INSERT INTO projets (id, nom) VALUES (13, 'Women Aren''t Funny');
INSERT INTO projets (id, nom) VALUES (14, 'Hey, Happy!');
INSERT INTO projets (id, nom) VALUES (15, 'You''re Next');
INSERT INTO projets (id, nom) VALUES (16, 'Birth of the Living Dead');
INSERT INTO projets (id, nom) VALUES (17, 'Ashes of Time (Dung che sai duk)');
INSERT INTO projets (id, nom) VALUES (18, 'Stolen (Stolen Lives)');
INSERT INTO projets (id, nom) VALUES (19, 'Annabel Takes a Tour (Annabel Takes a Trip)');
INSERT INTO projets (id, nom) VALUES (20, 'Molokai (Molokai: The Story of Father Damien)');
INSERT INTO projets (id, nom) VALUES (21, 'Tactical Force');
INSERT INTO projets (id, nom) VALUES (22, 'Hui Buh: The Castle Ghost');
INSERT INTO projets (id, nom) VALUES (23, 'Prime Suspect: The Lost Child');
INSERT INTO projets (id, nom) VALUES (24, 'Cop in Drag');
INSERT INTO projets (id, nom) VALUES (25, 'Hans (Kukkulan kuningas)');

/*--Table competences: 10 competences */
insert into competences (id, nom, description) values (1, 'Evidence', 'Focused 6th generation synergy');
insert into competences (id, nom, description) values (2, 'HLR', 'Streamlined responsive secured line');
insert into competences (id, nom, description) values (3, 'RC Detailing', 'Fundamental optimizing info-mediaries');
insert into competences (id, nom, description) values (4, 'Kurdish', 'Total 3rd generation matrix');
insert into competences (id, nom, description) values (5, 'BTS Installation', 'Seamless bottom-line access');
insert into competences (id, nom, description) values (6, 'Oil Industry', 'Down-sized homogeneous protocol');
insert into competences (id, nom, description) values (7, 'Lync', 'Fundamental systemic capability');
insert into competences (id, nom, description) values (8, 'Fixed Assets', 'Customer-focused homogeneous portal');
insert into competences (id, nom, description) values (9, 'Business Intelligence Tools', 'Universal attitude-oriented model');
insert into competences (id, nom, description) values (10, 'HBDI', 'Devolved composite concept');


insert into eleve_competence (id_competence,id_eleve, niveau) values (1,1,1),(1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (2,1,1),(2,2,1),(2,3,1),(2,4,1),(2,5,1),(2,6,1),(2,7,1),(2,8,1),(2,9,1),(2,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (3,1,1),(3,2,1),(3,3,1),(3,4,1),(3,5,1),(3,6,1),(3,7,1),(3,8,1),(3,9,1),(3,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (4,1,1),(4,2,1),(4,3,1),(4,4,1),(4,5,1),(4,6,1),(4,7,1),(4,8,1),(4,9,1),(4,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (5,1,1),(5,2,1),(5,3,1),(5,4,1),(5,5,1),(5,6,1),(5,7,1),(5,8,1),(5,9,1),(5,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (6,1,1),(6,2,1),(6,3,1),(6,4,1),(6,5,1),(6,6,1),(6,7,1),(6,8,1),(6,9,1),(6,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (7,1,1),(7,2,1),(7,3,1),(7,4,1),(7,5,1),(7,6,1),(7,7,1),(7,8,1),(7,9,1),(7,10,1);
insert into eleve_competence (id_competence,id_eleve, niveau) values (8,1,1),(8,2,1),(8,3,1),(8,4,1),(8,5,1),(8,6,1),(8,7,1),(8,8,1),(8,9,1),(8,10,1);